// Copyright 2017 by HarryOnline
// https://creativecommons.org/licenses/by/4.0/
//
// Show message screen

using Toybox.WatchUi as Ui;
using Toybox.Graphics as Gfx;

class MessageView extends Ui.View {

    var message;
    var writer;

    function initialize(msg) {
      View.initialize();
      writer = new WrapText();
      message = msg;
    }

    // Load your resources here
    function onLayout(dc) {
      setLayout(Rez.Layouts.MainLayout(dc));
    }

    // Update the view
    function onUpdate(dc) {
      View.onUpdate(dc);
      dc.setColor(Gfx.COLOR_WHITE, Gfx.COLOR_BLACK);
      var posY = dc.getHeight() / 3;
      posY = writer.writeLines(dc, message, Gfx.FONT_MEDIUM, posY);
    }

}
