// Copyright 2017 by HarryOnline
// https://creativecommons.org/licenses/by/4.0/
//
// Handle Select by scrolling down or loading new quote

using Toybox.WatchUi as Ui;
using Toybox.System;

class QuoteDelegate extends Ui.BehaviorDelegate {

  function initialize() {
    BehaviorDelegate.initialize();
  }

  function onSelect() {
    if (!writer.scrollDown()) {
      reader.load();
      writer.reset();
    }
    return true;
  }

}

