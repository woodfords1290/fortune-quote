// Copyright 2017 by HarryOnline
// https://creativecommons.org/licenses/by/4.0/
//
// Show splash screen to enable further Up/Down before loading

using Toybox.WatchUi as Ui;
using Toybox.Timer;

function startQuoteView() {
    Ui.switchToView(new QuoteView(), new QuoteDelegate(), 0);
}

class SplashView extends Ui.View {

    function initialize() {
        View.initialize();
    }

    // Load your resources here
    function onLayout(dc) {
        setLayout(Rez.Layouts.SplashLayout(dc));
        var myTimer = new Timer.Timer();
        myTimer.start(method(:startQuoteView), 1000, false);
    }

    // Update the view
    function onUpdate(dc) {
        // Call the parent onUpdate function to redraw the layout
        View.onUpdate(dc);
    }

}
